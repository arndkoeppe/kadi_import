# Installation

0. Ensure you have a python interpreter installed
1. Install library and dependencies with `pip install -e .`

# Usage

In the command prompt or bash execute:

```kadi-import-json-schema --file example.json```

```kadi-import-json-schema --file example.json --identifier my-identifier```

```kadi-import-json-schema --file example.json --template_type record```

For an explanation of available command-line arguments, execute:

```kadi-import-json-schema --help```

Tested with python 3.9 on Ubuntu Linux 20.04.

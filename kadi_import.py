# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import re
from copy import deepcopy
from pathlib import Path

import jsonref
from flask import json
from flask_login import current_user
from kadi_apy import KadiManager
from kadi_apy.lib import exceptions
from xmlhelpy import command
from xmlhelpy import option

def read_json_data(file):
    """Import a template from a JSON file.

    See :func:`read_json_data` for an explanation of the parameters and return value.
    """
    # Read json file
    file = Path(file)
    with file.open("r", encoding="utf-8") as f:
        return json.load(f)


STATIC_TYPE_MAPPING = {"integer": "int", "number": "float", "boolean": "bool"}


def _json_schema_to_extras(extras_schema, required=None):

    required = required or []

    def map_fun(key, value, required=True):
        # Key
        extra = {}
        if key is not None:
            extra["key"] = key

        # Default value
        default = value.get("default")
        if default is not None:
            extra["default"] = default

        # Original type and required
        value_type = value["type"]

        # Handle required
        if isinstance(value_type, (list, tuple)):
            if value_type[-1] is None:
                required = False
            value_type = value_type[0]

        # Map type
        if value_type == "object":
            result = _json_schema_to_extras(
                value["properties"], required=value.get("required")
            )
            extra["type"] = "dict"
            extra["value"] = result
        elif value_type == "array":
            result = _json_schema_to_extras([value["items"]])
            extra["type"] = "list"
            extra["value"] = result
        else:
            if value_type == "string":
                # String or date type
                if value.get("format") == "date-time":
                    extra["type"] = "date"
                else:
                    extra["type"] = "str"
            else:
                extra["type"] = deepcopy(STATIC_TYPE_MAPPING[value_type])

            # Validation
            validation = {}
            if required:
                validation["required"] = required

            choices = value.get("enum")
            if choices is not None:
                validation["options"] = list(choices)

            maximum = value.get("maximum")
            minimum = value.get("minimum")
            if maximum is not None or minimum is not None:
                validation["range"] = {"max": maximum, "min": minimum}

            if validation:
                extra["validation"] = validation

        return extra

    if isinstance(extras_schema, (list, tuple)):
        return [map_fun(None, value, required=False) for value in extras_schema]
    return [
        map_fun(key, value, required=key in required)
        for key, value in extras_schema.items()
    ]


def parse_json_schema_data(path, identifier=None, template_type="record"):
    """Import a template from a JSON Schema file.

    See :func:`read_json_schema_data` for an explanation of the parameters and return value.
    """
    # Read json file
    path = Path(path)
    with path.open("r", encoding="utf-8") as f:
        json_schema = jsonref.load(f)
    # Use schema and id to link / decide on processing?
    identifier = identifier or json_schema["$id"]
    identifier = re.sub("[^A-Za-z0-9-_]+", "--", identifier).lower()

    # Parse fixed record metadata from json schema root
    template_data = {
        "title": json_schema["title"],
        "identifier": identifier,
        "description": json_schema["description"],
    }

    # Parse properties
    extras = [
        {"key": "$schema", "type": "str", "value": json_schema["$schema"]},
    ]
    _id = json_schema.get("$id")
    if _id:
        extras.append({"key": "$id", "type": "str", "value": _id})

    extras.extend(
        _json_schema_to_extras(
            json_schema["properties"], required=json_schema.get("required")
        )
    )

    if template_type == "record":
        # TODO: need to add default values?
        # for key, value in record_properties.items():
        #     # Skip all falsy default values.
        #     if template_data["data"].get(key):
        #         value["default"] = template_data["data"][key]

        # Add the extra metadata to the record properties.
        template_data["type"] = "record"
        template_data["data"] = {"license": None, "tags": [], "extras": extras}

    elif template_type == "extras":
        template_data["type"] = "extras"
        template_data["data"] = extras

    return template_data


def read_import_data(path, file_format=None, identifier=None, template_type="record"):
    """Import a template from a file with given format."""
    if file_format is None:
        file_format = str(path).rsplit(".", 1)[-1]

    if file_format == "json":
        return parse_json_schema_data(
            path, identifier=identifier, template_type=template_type
        )

    return None


def _kadi_dtype(dtype):
    if dtype is not None:
        return type(dtype).__name__
    return "str"


def _json_to_kadi_datastructure(nested_dict):
    """Convert a nested dict to kadi data structure."""
    if isinstance(nested_dict, dict):
        kadi_list_of_dicts = []
        for key, value in nested_dict.items():
            value = deepcopy(value)
            ktype = _kadi_dtype(value)
            value = _json_to_kadi_datastructure(value)
            item = {"key": key, "type": ktype, "value": value}
            kadi_list_of_dicts.append(item)
        return kadi_list_of_dicts
    if isinstance(nested_dict, list):
        kadi_list_of_dicts = []
        for value in nested_dict:
            ktype = _kadi_dtype(value)
            value = _json_to_kadi_datastructure(value)
            item = {"type": ktype, "value": value}
            kadi_list_of_dicts.append(item)
        return kadi_list_of_dicts
    if nested_dict is None:
        return "None"
    return nested_dict


def fill_meta_data(new_data, kadi_meta_data=None, force=False):
    """Fill the new data in the kadi_meta_data."""
    updated_data = []
    if not kadi_meta_data:
        # No metadata template/record or empty
        updated_data.extend(new_data)
        return updated_data

    def fill_in(data, item, force):
        new_item = deepcopy(item)
        if new_item['type'] in ['list', 'dict']:
            if new_item['type'] == data['type']:
                new_item['value'] = fill_meta_data(data['value'], new_item['value'], force)
            else:
                print(f"The type of {data} does not satisfy {new_item['type']}")
        elif new_item['type'] == data['type']:
            # when the type satisfied
            if not new_item['value'] or force:
                new_item['value'] = data['value']
            else:
                print(f"{new_item} already exists, new data {data} will not be used.")
        else:
            # when the type is not satisfied
            print(f"The type of {data} does not satisfy {new_item['type']}")
        return new_item

    if "key" in kadi_meta_data[0].keys():
        new_data_index = {item["key"]: index for index, item in enumerate(new_data)}
        kadi_data_index = {item["key"]: index for index, item in enumerate(kadi_meta_data)}
        common_keys = new_data_index.keys() & kadi_data_index.keys()
        for key in common_keys:
            item = kadi_meta_data[kadi_data_index[key]]
            data = new_data[new_data_index[key]]
            updated_data.append(fill_in(data, item, force))
        for element in kadi_meta_data:
            if element["key"] not in common_keys:
                updated_data.append(element)
        for element in new_data:
            if element["key"] not in common_keys:
                updated_data.append(element)
    else:
        for data, item in zip(new_data, kadi_meta_data):
            updated_data.append(fill_in(data, item, force))
        if len(new_data) > len(kadi_meta_data):
            updated_data.extend(new_data[len(kadi_meta_data):])
        elif len(new_data) < len(kadi_meta_data):
            updated_data.extend(kadi_meta_data[len(new_data):])
    return updated_data


def fill_import_data(file_path, kadi_data, force):
    """
    Fill template/record with new data from file_path.

    Currently only the metadata is changed, description, license, tags, type are preserved.
    """
    file_content = read_json_data(file_path)
    new_metadata = _json_to_kadi_datastructure(file_content)
    output_data = {}
    if kadi_data is not None:
        output_data["description"] = deepcopy(kadi_data.get("description"))
        output_data["license"] = deepcopy(kadi_data.get("license"))
        output_data["tags"] = deepcopy(kadi_data.get("tags"))
        output_data["type"] = deepcopy(kadi_data.get("type"))
        meta_data = deepcopy(kadi_data.get("extras"))
        updated_metadata = fill_meta_data(new_metadata, meta_data, force)
    else:
        updated_metadata = fill_meta_data(new_metadata, None, force)
    output_data["extras"] = updated_metadata
    return output_data


@command(
    "kadi-import-template",
    version="0.1",
    description="Import an metadata schema into a Kadi Template.",
    example="""
        >>> kadi-import-metadata-schema --file example.xsd
        >>> kadi-import-metadata-schema --file example.json --identifier my-identifier
        """,
)
@option(
    "path",
    description="A file (xsd, json schema) with a metadata template.",
    required=True,
    char="p",
)
@option(
    "file_format",
    description="A file format, such as xsd or json for metadata templates.",
    default=None,
    required=False,
    char="f",
)
@option(
    "identifier",
    description="A Kadi identifier for the template.",
    default=None,
    required=False,
    char="i",
)
@option(
    "template_type",
    description="Type of template to create ('record' or 'extra')",
    default="record",
    required=False,
    char="t",
)
def kadi_import_template(path, file_format=None, identifier=None, template_type="record"):
    """Create a template based on json."""
    data = read_import_data(
        path,
        file_format=file_format,
        identifier=identifier,
        template_type=template_type,
    )
    manager = KadiManager()
    manager.template(create=True, **data)


@command(
    "kadi-fill",
    version="0.1",
    description="Fill the new data in a record/template/None",
)
@option(
    "file_path",
    description="Path of the json file with all needed data",
    required=True,
    char="p",
)
@option(
    "identifier",
    description="The Kadi identifier for the template or the record.",
    default=None,
    required=False,
    char="i",
)
@option(
    "new_identifier",
    description="A Kadi identifier for the new created record.",
    default=None,
    required=False,
    char="n",
)
@option(
    "force",
    description="Whether to change existing data with new data",
    default=None,
    required=False,
    char="f",
)
def kadi_fill(file_path, identifier=None, new_identifier=None, force=None):
    """Create a new record by filling the data in .json to a template/record/None."""
    manager = KadiManager()
    if not identifier:  # No record/template available
        kadi_data = None
        force = True
    else:
        try:
            kadi_data = manager.template(identifier=identifier, create=False).meta["data"]
            force = force or True  # template available
        except exceptions.KadiAPYInputError:
            kadi_data = manager.record(identifier=identifier, create=False).meta
            force = force or False  # record available
    data = fill_import_data(
        file_path,
        kadi_data,
        force=force
    )
    manager.record(identifier=new_identifier, create=True, **data)

if __name__ == "__main__":
    '''
    kadi_import_template.callback(
        "example.json", identifier="debug-extras-template1", template_type="extras"
    )
    '''
  
    kadi_import_template.callback(
        "example2.json", identifier="debug-record-template2", template_type="record"
    )

    kadi_import_template.callback(
        "datacite-metadata.json",
        # "http://schema.datacite.org/meta/kernel-4/metadata.xsd",
        identifier="debug-datacite-template",
        template_type="record",
    )

    # fill out a template
    kadi_fill.callback(
        "data2.json",
        identifier="debug-record-template2",
        new_identifier="debug-record-2"
    )

    # no template/record
    kadi_fill.callback(
        "data.json",
        new_identifier="26try15_empty"
    )

    # fill out a template
    kadi_fill.callback(
        "data.json",
        identifier="example3",
        new_identifier="26try15"
    )

    # fill out a record
    kadi_fill.callback(
        "data_copy.json",
        identifier="26try15",
        new_identifier="26try15_force",
        force=True
    )

    # fill out a record
    kadi_fill.callback(
        "data_copy.json",
        identifier="26try15",
        new_identifier="26try15_noforce",
    )
